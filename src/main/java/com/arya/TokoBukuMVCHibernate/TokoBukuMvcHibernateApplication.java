package com.arya.TokoBukuMVCHibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TokoBukuMvcHibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(TokoBukuMvcHibernateApplication.class, args);
	}

}
