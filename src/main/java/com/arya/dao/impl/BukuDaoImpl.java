package com.arya.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.arya.dao.BukuDao;
import com.arya.model.Buku;

@Repository
public class BukuDaoImpl implements BukuDao{
	@Autowired
	private SessionFactory session;
	
	
	public void add(Buku buku) {
		// TODO Auto-generated method stub
		session.getCurrentSession().save(buku);
	}

	public void edit(Buku buku) {
		// TODO Auto-generated method stub
		session.getCurrentSession().update(buku);
		
	}

	public void delete(int idBuku) {
		// TODO Auto-generated method stub
		session.getCurrentSession().delete(getBuku(idBuku));
	}

	public Buku getBuku(int idBuku) {
		return (Buku)session.getCurrentSession().get(Buku.class, idBuku);
	}

	public List getAllBuku() {
		// TODO Auto-generated method stub
		return session.getCurrentSession().createQuery("from Buku").list();
	}
	
}
