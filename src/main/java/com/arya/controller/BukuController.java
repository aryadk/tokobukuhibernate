package com.arya.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.arya.model.Buku;
import com.arya.service.BukuService;

@Controller
public class BukuController {
	@Autowired
	private BukuService bukuService;
	
	@RequestMapping("/index")
	public String setupForm(Map<String,Object> map) {
		Buku buku = new Buku();
		map.put("buku", buku);
		map.put("bukuList", bukuService.getAllBuku());
		return "buku";
	}
	
	@RequestMapping(value="/buku.do", method=RequestMethod.POST)
	public String doActions(@ModelAttribute Buku buku, BindingResult result, @RequestParam String action, Map<String, Object> map){
		Buku bukuResult = new Buku();
		String actions = action.toLowerCase();
		if (actions.equals("add") ) {
			bukuService.add(buku);
			bukuResult = buku;
		}else if(actions.equals("edit")) {
			bukuService.edit(buku);
			bukuResult = buku;
		}else if(actions.equals("delete")) {
			bukuService.delete(buku.getIdBuku());
			bukuResult = new Buku();
		}else {
			Buku searchedBuku = bukuService.getBuku(buku.getIdBuku());
			bukuResult = searchedBuku!=null ? searchedBuku : new Buku();
		}
		map.put("buku", bukuResult);
		map.put("bukuList", bukuService.getAllBuku());
		return "buku";
	}
}
