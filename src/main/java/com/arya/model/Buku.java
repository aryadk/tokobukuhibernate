package com.arya.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_buku")
public class Buku {
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idBuku;
	@Column
	private String judul;
	@Column
	private int harga;
	
	public Buku() {}
	public Buku(int idBuku,String judul,int harga) {
		super();
		this.idBuku = idBuku;
		this.judul = judul;
		this.harga = harga;
	}
	public int getIdBuku() {
		return idBuku;
	}
	public void setIdBuku(int idBuku) {
		this.idBuku = idBuku;
	}
	public String getJudul() {
		return judul;
	}
	public void setJudul(String judul) {
		this.judul = judul;
	}
	public int getHarga() {
		return harga;
	}
	public void setHarga(int harga) {
		this.harga = harga;
	}
	
	
}
