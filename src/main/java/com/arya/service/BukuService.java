package com.arya.service;

import java.util.List;

import com.arya.model.Buku;

public interface BukuService {
	public void add(Buku buku);
	public void edit(Buku buku);
	public void delete(int idBuku);
	public Buku getBuku(int idBuku);
	public List getAllBuku();
}
