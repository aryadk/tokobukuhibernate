package com.arya.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arya.dao.BukuDao;
import com.arya.model.Buku;
import com.arya.service.BukuService;

@Service
public class BukuServiceImpl implements BukuService{
	@Autowired
	private BukuDao bukuDao;

	@Transactional
	public void add(Buku buku) {
		// TODO Auto-generated method stub
		bukuDao.add(buku);
		
	}

	@Transactional
	public void edit(Buku buku) {
		// TODO Auto-generated method stub
		bukuDao.edit(buku);
	}
	
	@Transactional
	public void delete(int idBuku) {
		// TODO Auto-generated method stub
		bukuDao.delete(idBuku);
	}

	@Transactional
	public Buku getBuku(int idBuku) {
		// TODO Auto-generated method stub
		return bukuDao.getBuku(idBuku);
	}

	@Transactional
	public List getAllBuku() {
		// TODO Auto-generated method stub
		return bukuDao.getAllBuku();
	}
}
