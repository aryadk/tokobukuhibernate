<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/jsp/includes.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Student Management</title>
</head>
<body>
<h1>Students Data</h1>
<form:form action="buku.do" method="POST" commandName="buku">
	<table>
		<tr>
			<td>Buku ID</td>
			<td><form:input path="idBuku" /></td>
		</tr>
		<tr>
			<td>Judul</td>
			<td><form:input path="judul" /></td>
		</tr>
		<tr>
			<td>Harga</td>
			<td><form:input path="harga" /></td>
		</tr>
		
		<tr>
			<td colspan="2">
				<input type="submit" name="action" value="Add" />
				<input type="submit" name="action" value="Edit" />
				<input type="submit" name="action" value="Delete" />
				<input type="submit" name="action" value="Search" />
			</td>
		</tr>
	</table>
</form:form>
<br>
<table border="1">
	<th>ID</th>
	<th>Judul</th>
	<th>Harga</th>
	<c:forEach items="${bukuList}" var="buku">
		<tr>
			<td>${buku.idBuku}</td>
			<td>${buku.judul}</td>
			<td>${buku.harga}</td>
		</tr>
	</c:forEach>
</table>
</body>
</html>